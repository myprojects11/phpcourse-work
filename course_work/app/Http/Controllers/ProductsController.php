<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $reqest)
    {
        //
        $products = Product::with(['tags','tags.param','category','image'])->get();
//        print_r($products->first());die;
        return view('products/index',['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $categories = DB::table('categories')->get();

        return view('products/create_product_form',['categories'=>$categories,'cur_tags' => null]);
    }

    public function onchange(Request $request, $cat = null)
    {
        $categories = DB::table('categories')->get();


        if($cat != '')
        {
            $tags = DB::table('tags')
                ->join('params','params.id','=','tags.param_id')
                ->join('categories','categories.id','=','params.category_id')
                ->select('tags.title as tag_title','categories.title as category_name','tags.id')
                ->where('categories.title','=',"$cat")
                ->get();
//
            $data = [$request->input(), 'cat' => $cat];
            return view('products/create_product_form',['categories'=>$categories,'cur_tags' => $tags,'cat' => $cat]);
        }

        print_r($cat);die;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $data = null)
    {
        //

        print_r($request->all());

        return redirect()->route('product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product,Request $request)
    {
        //
        $item = Product::where('products.id','=',$product->id)->get();
//        print_r($item->first()->id);die;
        return view('products/item',['product' => $item->first()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }



}
