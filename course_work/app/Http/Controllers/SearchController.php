<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    //
    /**
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = DB::table('categories')->get();
        $q = $request->input('query');
        if($q != null && $q != '')
        {
            $query = DB::table('categories')->where('title','=',$q)->select('id')->get();
            $query_id = $query->first()->id;
            $search = Product::with(['tags','tags.param'])
                ->where('category_id','=',$query_id)
                ->get();
//            print_r($search);die;
            $tags = DB::table('tags')->get();
            return view('products/index',['products' => $search,'categories'=>$categories,'tags' => $tags]);
        }
//        return view('products/index',['Have no items','asd']);
    }
}
