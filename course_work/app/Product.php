<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $primaryKey = 'id';
    public $isIncrement = true;
    public $timestamps = true;

    public function category()
    {
        # code...
        return $this->belongsTo('App\Category');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag','tags_products');
    }

    public function image()
    {
        return $this->belongsTo('App\Image');
    }

}
