<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    public function products()
    {
        return $this->belongsToMany('App\Param','tags_products');
    }

    public function param()
    {
        return $this->belongsTo('App\Param');
    }
}
