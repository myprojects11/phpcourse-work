<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagProduct extends Model
{
    //
    public function tagsProducts()
    {
        return $this->belongsToMany('App\Product');
    }
}
