<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class SidebarProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

//            echo "Here";die;
            View::composer('*',function ($view){
            $categories = DB::table('categories')->get();
            $tags = DB::table('tags')->get();
            $view->with(['categories'=>$categories,'tags' => $tags]);
        });
    }
}
