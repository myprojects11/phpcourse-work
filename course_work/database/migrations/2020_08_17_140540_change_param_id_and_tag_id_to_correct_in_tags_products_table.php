<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeParamIdAndTagIdToCorrectInTagsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tags_products', function (Blueprint $table) {
            //
            $table->renameColumn('productId', 'product_id');
            $table->renameColumn('tagId', 'tag_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tags_products', function (Blueprint $table) {
            //
            $table->renameColumn('product_id', 'productId');
            $table->renameColumn('tag_id', 'tagId');
        });
    }
}
