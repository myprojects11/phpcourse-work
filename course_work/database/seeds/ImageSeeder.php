<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;


class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        chdir('public\images');
        $dirs = scandir(getcwd());
        for($i = 2; $i<count($dirs) - 1; $i++)
        {
            chdir($dirs[$i]);
            $files = scandir(getcwd());

            for ($j = 2;$j<count( $files);$j++)
            {
                $last = basename(getcwd());

//                echo $last."\\".scandir(getcwd())[$j] . "\n";
                DB::table('images')
                    ->insert([
                        'file' => $last."\\".scandir(getcwd())[$j],
                        'name' => scandir(getcwd())[$j],
                        'alt' =>  preg_replace('/\\.[^.\\s]{3,4}$/', '',scandir(getcwd())[$j]),
                ]);
            }
            chdir('..');

        }
    }
}
