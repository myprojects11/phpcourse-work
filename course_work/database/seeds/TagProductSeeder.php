<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class TagProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function write_tags_products($product_id,$tag_id)
    {
        DB::table('tags_products')->insert([
            'product_id' => $product_id,
            'tag_id' => $tag_id,
        ]);
    }

    public function run()
    {
        for($i=1;$i<=100;$i++)
        {
            $tmp1 = random_int(1,4);
            //
            switch ($tmp1)
            {
                case 1:
                    $this->write_tags_products($i, 1);
                    $this->write_tags_products($i, 3);
                break;

                case 2:
                    $this->write_tags_products($i, 2);
                    $this->write_tags_products($i, 4);
                break;

                case 3:
                    $this->write_tags_products($i, 5);
                    $this->write_tags_products($i, 7);
                break;

                case 4:
                    $this->write_tags_products($i, 6);
                    $this->write_tags_products($i, 8);
                break;

                default:
                    $this->write_tags_products($i, 1);
                    $this->write_tags_products($i, 3);
            }
        }
    }
}




