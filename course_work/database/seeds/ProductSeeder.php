<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{

    public function add_tags($product_id, $tag_id)
    {
        DB::table('tags_products')->insert([
            'product_id' => $product_id,
            'tag_id' => $tag_id,
        ]);
        echo "p=$product_id & t=$tag_id \n";
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $name_array = [
            'Telephones' => [
                'Samsung'   => [
                    'Name'  => 'Samsung',
                    'Model' => ['Galaxy 9','Galaxy 8','Galaxy 11','A9','A13'],
                    'Price' => [450, 420, 500, 300, 450],
                    'Desc'  => [
                        'Best phone with best design',
                        'The faster & the stronger phone of 2016',
                        'Best android phone of 2020',
                        'Best your choice',
                        'So cheap & force',
                        ],
                    'Image' =>[13, 12, 11, 10, 9],
                ],
                'Apple'     => [
                    'Name'  => 'Apple',
                    'Model' => ['IPhone 11','IPhone 10','IPhone 9'],
                    'Price' => [1000, 620, 500],
                    'Desc'  => [
                        'Very expensive but very force',
                        'Classic IPhone',
                        'Best Phone in my life',
                    ],
                    'Image' =>[15, 14, 16],
                ],
            ],
            'Computers' => [
                'ACER'      => [
                    'Name'  => 'ACER',
                        'Model' => ['Nitro 5','Nitro 9','Predator 2','Predator 1','Aspire 9'],
                    'Price' => [450, 1200, 600, 750, 500],
                    'Desc'  => [
                        'Very productive',
                        'The faster & the stronger computer',
                        'Good PC for your needs',
                        'Best your choice',
                        'So cheap & force',
                    ],
                    'Image' =>[2, 3, 5, 4, 1],
                ],
                'ASUS'      => [
                    'Name'  => 'ASUS',
                    'Model' => ['ROG Strix','Laptop','ZenBook'],
                    'Price' => [600, 520, 500],
                    'Desc'  => [
                        'The faster & the stronger computer',
                        'Good PC for your needs',
                        'So cheap & force',
                    ],
                    'Image' =>[6, 7, 8],
                ],
            ]
        ];

        for($i=1;$i<=100;$i++)
        {
            $category = random_int(3,4);
//            $category = 3;
            switch ($category)
            {
                case 4:
                    $rnd = random_int(1,2);
                    switch ($rnd)
                    {
                        case 1:
                            $tel = $name_array['Telephones']['Samsung'];
                            $model = random_int(0,4);
                            DB::table('products')
                                ->insert([
                                    'name' => "{$tel['Name']} {$tel['Model'][$model]}",
                                    'model' => "{$tel['Model'][$model]}",
                                    'price' => $tel['Price'][$model],
                                    'description' => "{$tel['Desc'][$model]}",
                                    'category_id' => $category,
                                    'image_id' => $tel['Image'][$model],
                                ]);

                            $this->add_tags($i,6);
                            $this->add_tags($i,8);
                        break;

                        case 2:
                            $tel = $name_array['Telephones']['Apple'];
                            $model = random_int(0,2);
                            DB::table('products')
                                ->insert([
                                    'name' => "{$tel['Name']} {$tel['Model'][$model]}",
                                    'model' => "{$tel['Model'][$model]}",
                                    'price' => $tel['Price'][$model],
                                    'description' => "{$tel['Desc'][$model]}",
                                    'category_id' => $category,
                                    'image_id' => $tel['Image'][$model],
                                ]);
                            $this->add_tags($i,5);
                            $this->add_tags($i,7);
                        break;
                    }
                break;

                case 3:
                    $rnd = random_int(1,2);
                    switch ($rnd)
                    {
                        case 1:
                            $tel = $name_array['Computers']['ACER'];
                            $model = random_int(0,4);
                            DB::table('products')
                                ->insert([
                                    'name' => "{$tel['Name']} {$tel['Model'][$model]}",
                                    'model' => "{$tel['Model'][$model]}",
                                    'price' => $tel['Price'][$model],
                                    'description' => "{$tel['Desc'][$model]}",
                                    'category_id' => $category,
                                    'image_id' => $tel['Image'][$model],
                                ]);

                            $this->add_tags($i,2);
                            $this->add_tags($i,4);
                        break;

                        case 2:
                            $tel = $name_array['Computers']['ASUS'];
                            $model = random_int(0,2);
                            DB::table('products')
                                ->insert([
                                    'name' => "{$tel['Name']} {$tel['Model'][$model]}",
                                    'model' => "{$tel['Model'][$model]}",
                                    'price' => $tel['Price'][$model],
                                    'description' => "{$tel['Desc'][$model]}",
                                    'category_id' => $category,
                                    'image_id' => $tel['Image'][$model],
                                ]);

                            $this->add_tags($i,1);
                            $this->add_tags($i,3);
                        break;
                    }
                        break;

                default:
                    break;
            }
        }
    }
}
