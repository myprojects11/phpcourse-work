@extends('layouts.app')

@section('content')
    <img src="{{url("images/{$product->image->file}")}}" style="width: 20%;height: 20%">
    <div class="container">
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ __('Name') }}</label>

            <div class="col-md-6">
                <label for="name" class="col-md-4 col-form-label text-md-left text-light">{{ $product->name }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ __('Model') }}</label>

            <div class="col-md-6">
                <label for="name" class="col-md-4 col-form-label text-md-left text-light">{{ $product->model }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ __('Price') }}</label>

            <div class="col-md-6">
                <label for="name" class="col-md-4 col-form-label text-md-left text-light">{{ "$product->price$" }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ __('Description') }}</label>

            <div class="col-md-6">
                <label for="name" class="col-md-4 col-form-label text-md-left text-light">{{ "$product->description" }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ __('Category') }}</label>

            <div class="col-md-6">
                <label for="name" class="col-md-4 col-form-label text-md-left text-light">{{ $product->category->title }}</label>
            </div>
            @foreach($product->tags as $tag)
                <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ $tag->param['title']}}</label>

                <div class="col-md-6">

                    <label for="name" class="col-md-4 col-form-label text-md-left text-light">{{$tag['title']}}</label>
                </div>
            @endforeach
        </div>
    </div>
@endsection
