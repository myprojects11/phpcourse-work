@extends('layouts.app')

@section('content')
    @guest
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>

            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        </ul>
    @else
        <div class="container">

            <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                @csrf
{{--                <span>{{print_r($cat)}}</span>--}}
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="n" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="model" class="col-md-4 col-form-label text-md-right text-light">{{ __('Model') }}</label>

                    <div class="col-md-6">
                        <input id="model" type="text" class="form-control @error('model') is-invalid @enderror" name="m" value="{{ old('model') }}" required autocomplete="model" autofocus>

                        @error('model')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right text-light">{{ __('Price') }}</label>

                    <div class="col-md-6">
                        <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="p" value="{{ old('price') }}" required autocomplete="price" autofocus>

                        @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
{{--                @php--}}
{{--                    print_r($categories);--}}
{{--                @endphp--}}

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right text-light">{{ __('Category') }}</label>

                    <div class="col-md-6 d-flex">
                        <div class="dropdown">
                            <button name="c" value="Category" class="btn btn-secondary dropdown-toggle" type="button" id="category-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $cat ?? 'Categories' }}
                            </button>
                            <div class="dropdown-menu bg-secondary" aria-labelledby="category-btn">
                                @foreach($categories as $category)
                                    @if($category->parent_id == 0)
                                        <li class="active">
                                            <a class="dropdown-item bg-secondary text-light" id="cat-{{$category->id}}" href="{{ route('product.onchange',['cat' => $category->title]) }}">{{$category->title}}</a>
                                            <ul class="collapse list-unstyled" id="cat-{{$category->id}}" style='margin-bottom: 0 !important;'>
                                                @foreach($categories as $child_category)
                                                    @if($category->id == $child_category->parent_id)
                                                        <a id="cat-{{$child_category->id}}" type="submit" href="{{ route('product.onchange',['cat' => $child_category->title]) }}" class="dropdown-item bg-secondary bg-darker-1 text-light" style="padding-left: calc(var(--link-default-padding) + 15px) !important;">
                                                            {{$child_category->title}}
                                                        </a>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Tags') }}</label>
                    <div class="col-md-6 d-flex">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="tag-1-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Tags
                            </button>

                            <div class="dropdown-menu bg-secondary" aria-labelledby="tag-1-btn">
                                @if($cur_tags != null)
                                    @foreach($cur_tags as $tag)
                                        <div class="dropdown-item bg-secondary text-light">
                                            <input id="tag-val-{{$tag->id}}" type="text" name="t-{{$tag->id}}" class="bg-secondary text-light" hidden="true" >
                                            <input id="cb-{{$tag->id}}" type="checkbox" class="bg-secondary text-light" onchange="setTags('{{$tag->tag_title}}','cb-{{$tag->id}}','tag-val-{{$tag->id}}')" value="{{$tag->tag_title}}" >
                                            <a href="#"  class="bg-secondary bg-darker-1 text-light" id="tag-{{$tag->id}}" href="{{'#'}}" onclick="change('tag-{{$tag->id}}','tag-1-btn')">
                                                {{$tag->tag_title}}
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="image" class="col-md-4 col-form-label text-md-right text-light">{{ __('Image') }}</label>

                    <div class="col-md-6">
                        <input type="file" name="image">


                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-md-4 col-form-label text-md-right text-light">{{ __('Description') }}</label>

                    <div class="col-md-6">
                        <textarea id="description" type="text" rows="7" class="form-control @error('description') is-invalid @enderror" name="des" value="{{ old('description') }}" style="resize: none;" required autocomplete="description" autofocus>
                        </textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <button type="submit" class="btn btn-primary btn-lg col-md-6" style=" margin-left: 33.5%;" >Submit</button>
                </div>
            </form>
        </div>

        <script type="text/javascript">
            function change(e,b){
                // console.log(e,b);

                text = document.getElementById(`${e}`).textContent;
                btn = document.getElementById(b);
                inp = document.getElementById('category');

                // console.log(e,b);
                // console.dir(btn);

                btn.textContent = text.replace(/[\n\r]+|[\s]{2,}/g, ' ').trim();
                btn.value = btn.textContent;
                inp.value = btn.textContent;
            }

            function tmp()
            {

            }


        </script>
    @endguest
@endsection
