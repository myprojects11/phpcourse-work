@extends('layouts.app')

@section('content')
    @if($products->first()['id'] != null)
        @foreach ($products as $product)
            @if($product != null || $product != '')
                <div class="card" style="width: 18rem;margin: 0 0.5rem 1rem">
                    <img class="card-img-top" src="{{$product->image == null ? url('/images/placeholder.png') : url("/images/{$product->image->file}")}}" alt="Card image cap">
                    <div class="card-body" style="display: flex;flex-direction: column;justify-content: flex-end;">
                        <h5 class="card-title ">{{$product->name}}</h5>
                        <p class="card-text text-dark">Category: {{$product->category['title']}}</p>
                        @foreach($product->tags as $tag)
                            <p class="card-text text-dark">{{$tag->param['title']}}: {{$tag['title']}}</p>
                        @endforeach
                    </div>
                    <a href="{{route('product.show',$product->id)}}" class="btn btn-primary">Go somewhere</a>
                </div>
            @endif
        @endforeach
    @else
        <h1 class="text-light">Have no items</h1>
    @endif
@endsection

@section('sidebar')
    <nav id="sidebar" class="bg-dark bg-darker-3">
        <div class="sidebar-header bg-dark">
            <button type="button" id="sidebarCollapse" class="btn btn-info" style="width: 100%;" onclick="sidebarToggle()">
                <i class="fas fa-align-left"></i>
                <span id="btn-toggle-span">Toggle Sidebar</span>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify"></i>
            </button>
        </div>

        <ul class="list-unstyled components">
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-home"></i>
                    Categories
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    @foreach($categories as $category)
                        @if($category->parent_id == 0 )
                            <li class="active">
                                <a href="#cat-{{$category->id}}" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed">
                                    {{$category->title}}
                                </a>
                                <ul class="list-unstyled collapse" id="cat-{{$category->id}}">
                                    @foreach($categories as $child_category)
                                        @if($category->id == $child_category->parent_id)
                                            <li>
                                                <a href="{{route('search.index',['_token='=>csrf_token(),'query' => $child_category->title])}}" aria-expanded="false" class="bg-darker-3" style="padding-left: calc(var(--link-default-padding) + 15px) !important;">
                                                        {{$child_category->title}}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </li>
            <li>
                <a href="#tagsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Tags
                </a>
                <ul class="collapse list-unstyled" id="tagsSubmenu">
                    @foreach($tags as $tag)
                    <li>
                        <a href="#" aria-expanded="false" class="bg-darker-3" style="padding-left: calc(var(--link-default-padding) + 15px) !important;">
                            <i class="fas fa-hashtag"></i>{{$tag->title}}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-question"></i>
                    FAQ
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-paper-plane"></i>
                    Contact
                </a>
            </li>
        </ul>
    </nav>
@endsection
