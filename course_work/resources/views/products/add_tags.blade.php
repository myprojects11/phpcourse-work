@extends('layouts.app')

@section('content')
    @guest
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>

            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        </ul>
    @else
        <div class="container">

            <form action="{{ route('product.store',['data' => $data]) }}" method="post" enctype="multipart/form-data">
                @csrf
                @php
                    print_r($data);
                @endphp
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" readonly="true" class="form-control @error('name') is-invalid @enderror" name="" value="{{ $data['n'] }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="model" class="col-md-4 col-form-label text-md-right">{{ __('Model') }}</label>

                    <div class="col-md-6">
                        <input id="model" type="text" readonly="true" class="form-control @error('model') is-invalid @enderror" name="" value="{{ $data['m'] }}" required autocomplete="model" autofocus>

                        @error('model')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

                    <div class="col-md-6">
                        <input id="price" type="text" readonly="true" class="form-control @error('price') is-invalid @enderror" name="" value="{{ $data['p'] }}" required autocomplete="price" autofocus>

                        @error('price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

                    <div class="col-md-6">
                        <input id="category" readonly="true" type="text" class="form-control @error('category') is-invalid @enderror" name="" value="{{ $data['c'] }}" required autocomplete="category" autofocus>

                        @error('category')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <button type="submit" class="btn btn-primary btn-lg col-md-6" style=" margin-left: 33.5%;" >Submit</button>
                </div>
            </form>
        </div>
    @endguest

    <script type="text/javascript">
        function setTags(val,id,inp_id)
        {
            el = document.getElementById(id);
            val_el = document.getElementById(inp_id);

            console.log(val_el);
            if(el.checked == true){
                val_el.setAttribute('value',val);
                console.log(val_el.value);
            }else {
                val_el.setAttribute('value','');
            }
        }
    </script>
@endsection
