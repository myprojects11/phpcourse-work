@extends('layouts.app')

@section('content')
    @guest
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>

            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        </ul>
    @else
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ Auth::user()->name }}</div>
                        @if(Route::has('login'))
                            @foreach ($users as $user)
                                <div class="card-body"><span>{{$user->id}} -- {{$user->email}}</span></div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection
