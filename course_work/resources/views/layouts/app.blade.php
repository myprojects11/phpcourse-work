<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style4.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <style>
        .dropdown .dropdown-menu .active:hover > ul{
            display: block;
        }

    </style>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.scss') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="bg-dark">
        <nav class="navbar n-b-margin navbar-expand-md navbar-dark bg-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    EasyBuy
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <form class="form-inline" METHOD="post" action="{{route('search.index')}}">
                        @csrf
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="q">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <a href="{{ route('user.index') }}" class="dropdown-item">User Managment</a>

                                    <a href="{{ route('product.create') }}" class="dropdown-item">Create Product</a>

                                    <a href="{{ route('product.index') }}" class="dropdown-item">Products</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>


        <div class="wrapper bg-dark">
            <!-- Sidebar  -->
{{--            @yield('sidebar')--}}

            <nav id="sidebar" class="bg-dark bg-darker-3">
                <div class="sidebar-header bg-dark">
                    <button type="button" id="sidebarCollapse" class="btn btn-info" style="width: 100%;" onclick="sidebarToggle()">
                        <i class="fas fa-align-left"></i>
                        <span id="btn-toggle-span">Toggle Sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>

                <ul class="list-unstyled components">
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-home"></i>
                            Categories
                        </a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            @foreach($categories as $category)
                                @if($category->parent_id == 0 )
                                    <li class="active">
                                        <a href="#cat-{{$category->id}}" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed">
                                            {{$category->title}}
                                        </a>
                                        <ul class="list-unstyled collapse" id="cat-{{$category->id}}">
                                            @foreach($categories as $child_category)
                                                @if($category->id == $child_category->parent_id)
                                                    <li>
                                                        <a href="{{route('search.index',['_token='=>csrf_token(),'query' => $child_category->title])}}" aria-expanded="false" class="bg-darker-3" style="padding-left: calc(var(--link-default-padding) + 15px) !important;">
                                                            {{$child_category->title}}
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <a href="#tagsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fas fa-copy"></i>
                            Tags
                        </a>
                        <ul class="collapse list-unstyled" id="tagsSubmenu">
                            @foreach($tags as $tag)
                                <li>
                                    <a href="#" aria-expanded="false" class="bg-darker-3" style="padding-left: calc(var(--link-default-padding) + 15px) !important;">
                                        <i class="fas fa-hashtag"></i>{{$tag->title}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fas fa-question"></i>
                            FAQ
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fas fa-paper-plane"></i>
                            Contact
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- Page Content  -->
            <div id="content" class="bg-secondary" style="padding-top: 60px;border-radius: 10px; ">

                <main class="d-flex justify-content-center flex-wrap">
                    @yield('content','products/index')
                </main>
            </div>
        </div>


    </div>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        function sidebarToggle(){

            ebtn = document.getElementById('sidebarCollapse');
            esid = document.getElementById('sidebar');
            espan = document.getElementById('btn-toggle-span');
            sidebarActive = esid.classList.toggle('active')

            if(sidebarActive){
                espan.textContent="";
            }
            else {
                espan.textContent="Toggle Sidebar";
            }
        }
    </script>


</body>
</html>
