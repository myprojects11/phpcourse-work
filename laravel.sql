-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2020 at 03:42 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Ноутбуки и компьютеры', 0, NULL, NULL),
(2, 'Электроника', 0, NULL, NULL),
(3, 'Ноутбуки', 1, NULL, NULL),
(4, 'Телефоны', 2, NULL, NULL),
(5, 'Сад', 0, NULL, NULL),
(6, 'Одежда', 0, NULL, NULL),
(9, 'Инструменты', 5, NULL, NULL),
(12, 'Мужская', 6, NULL, NULL),
(13, 'Женская', 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `Id` int(11) NOT NULL,
  `file` longblob NOT NULL,
  `name` varchar(256) NOT NULL,
  `alt` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_10_124530_create_product_table', 1),
(12, '2020_08_10_153901_create_tags_table', 2),
(14, '2020_08_10_165311_create_categories_table', 2),
(15, '2020_08_10_165514_uppdate_products_table', 3),
(16, '2020_08_10_170617_create_tags_products_table', 4),
(17, '2020_08_10_170945_update_add_param_id_tags_table', 5),
(19, '2020_08_10_164826_create_params_table', 6),
(20, '2020_08_17_140011_change_param_id_to_correct_tags_table', 7),
(21, '2020_08_17_140540_change_param_id_and_tag_id_to_correct_in_tags_products_table', 7),
(22, '2020_08_17_141003_rename_column_in_products_table', 7),
(23, '2020_08_17_141108_rename_column_in_params_table', 7),
(24, '2020_08_17_141351_rename_column_in_categories_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `params`
--

CREATE TABLE `params` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `params`
--

INSERT INTO `params` (`id`, `title`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Процессор', 3, NULL, NULL),
(2, 'Производитель', 3, NULL, NULL),
(3, 'Производитель', 4, NULL, NULL),
(4, 'Диагональ экрана', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `image_id` longblob NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `model`, `price`, `image_id`, `description`, `created_at`, `updated_at`, `category_id`) VALUES
(1, 'UAYDc', 'AD-Pg7DAI', '257.00', '', 'BTBpcdZAzr8X9XRT2oh7', NULL, NULL, 2),
(2, 'WSQy1', 'AD-nZdfyw', '612.00', '', 'IdoONBGW3Pi873UgRGiX', NULL, NULL, 4),
(3, 'RwofA', 'AD-Dln1MS', '571.00', '', '7HAriUYMVGLPRVa58E6I', NULL, NULL, 4),
(4, 'Weoo7', 'AD-8A8NRr', '898.00', '', 'DDcMS4kQOUaRn2mR28E8', NULL, NULL, 2),
(5, 'lerjW', 'AD-LUEEC7', '320.00', '', 'sFB4U2YdTyjn62qHsZgg', NULL, NULL, 3),
(6, 'gBrgh', 'AD-GhRPma', '534.00', '', 'qXGhpO1Owg4qX6FYl7Kl', NULL, NULL, 1),
(7, 'a2cXY', 'AD-Gl6YsV', '805.00', '', '5xa3b1SftKXqCFkXTudx', NULL, NULL, 4),
(8, 'jTf1U', 'AD-muUeat', '648.00', '', '1FpCWdLarzT1QmY5v4km', NULL, NULL, 2),
(9, 'hQZgO', 'AD-uXB50u', '550.00', '', 'BullgslYMx9zO5cKDfoQ', NULL, NULL, 2),
(10, 'PcpwN', 'AD-UaGR2G', '123.00', '', 'bqbFC1dLSg3xNUGqNt0g', NULL, NULL, 2),
(11, 'X5nUC', 'AD-nlgFkV', '826.00', '', 'XUhf563kZ4uVV2fBZBsB', NULL, NULL, 2),
(12, 'wSxQ5', 'AD-xMg93h', '439.00', '', 'JzmU77UNr7Fk7h2etr8Z', NULL, NULL, 3),
(13, 'pMAKP', 'AD-S6Wfwn', '156.00', '', 'qcVkR96APud6cBF9J8WQ', NULL, NULL, 4),
(14, 'LxMQS', 'AD-8udh2z', '467.00', '', 'ReDwZJWbVnvWiTXv4The', NULL, NULL, 1),
(15, 'XDZhQ', 'AD-Bccj0J', '517.00', '', 'dnEESQUCWsGnn9R3APbv', NULL, NULL, 1),
(16, 'Ew5bd', 'AD-qT5tWX', '583.00', '', '4uOa2NnG2Rpi2NmKS9Pb', NULL, NULL, 1),
(17, '7Csgy', 'AD-w9OcrP', '764.00', '', 'FakgW6qTXftXqDVnnY8G', NULL, NULL, 1),
(18, 'pcwNw', 'AD-q64n20', '378.00', '', 'ak93lv8xt1nNgjzlu3jo', NULL, NULL, 3),
(19, 'smpS7', 'AD-SVLBKQ', '920.00', '', 'uP4qWGIToPrik8kBk4dZ', NULL, NULL, 3),
(20, 'IFoio', 'AD-KB9sRV', '142.00', '', 'Pk947e56c0TlWTymse1b', NULL, NULL, 1),
(21, 'cDqes', 'AD-iCJgzY', '983.00', '', 'tlcK8V71SaxxifP3i8s8', NULL, NULL, 4),
(22, 'oftsf', 'AD-3tKZTN', '295.00', '', 'anQAt5iYHv9OZPhwkXjM', NULL, NULL, 2),
(23, 'fdHsV', 'AD-CLzG2Q', '948.00', '', 'jRKG61cHhtaAH7Qb52xn', NULL, NULL, 2),
(24, 'QXsCF', 'AD-oZtuTn', '409.00', '', '0jSPHak0gFPs3xKj2ZK5', NULL, NULL, 2),
(25, '6azUl', 'AD-xefgIn', '728.00', '', '67acAlLk6S4VNxc5CN4q', NULL, NULL, 2),
(26, 'CZjLg', 'AD-Alomsq', '286.00', '', 'SSBBjnRqRrubgy6eaqaP', NULL, NULL, 4),
(27, 'OqTFg', 'AD-XcEPUp', '278.00', '', 'wZdFwOTiMMEHjUtvjsuF', NULL, NULL, 4),
(28, 'W7Shq', 'AD-Z4F95n', '844.00', '', 'XVG3q9eyeUB0e0vzPgQr', NULL, NULL, 3),
(29, 'eCwfo', 'AD-Pe754P', '438.00', '', 'W62e6GikjsT3YccOVUzT', NULL, NULL, 2),
(30, '3LIn4', 'AD-Voko1U', '856.00', '', 'XmM894M4bKIjFeDnnMea', NULL, NULL, 4),
(31, '2FeuN', 'AD-suqiiY', '142.00', '', 'Qb1f1nnMX5gaRaRaNsji', NULL, NULL, 2),
(32, 'pwFBm', 'AD-eBGGAe', '444.00', '', 'UrjKD7BwO1vW8LcXm6ON', NULL, NULL, 3),
(33, 'jE8Ds', 'AD-0zpXwj', '241.00', '', '4TI8LqnMflzEaLakz6MK', NULL, NULL, 1),
(34, 'HxSIO', 'AD-wlqbE8', '268.00', '', '9yhLMXVMq5OpiqVSDjln', NULL, NULL, 1),
(35, 'zHAon', 'AD-hJuR7p', '214.00', '', 'MwuiV1iEiahokddnpsfC', NULL, NULL, 2),
(36, 'qANXF', 'AD-hQJB3B', '317.00', '', 'In3ktmDvVhoJJkmZYstQ', NULL, NULL, 1),
(37, '0U422', 'AD-mGAOUT', '149.00', '', 'qGzx1MhDmh5l1OSGTiac', NULL, NULL, 1),
(38, 'kvopd', 'AD-yhD4QR', '346.00', '', 'uuzNGOLg5QJsGJIis0WF', NULL, NULL, 3),
(39, 'aBdal', 'AD-0RnEh2', '189.00', '', 'dMNC4HyYGndjYUApInNm', NULL, NULL, 2),
(40, 'Ztzus', 'AD-4I1GmO', '596.00', '', 'isAEC85d2jvsco1IZMzH', NULL, NULL, 1),
(41, 'yuCvq', 'AD-cQCJk0', '686.00', '', 'rmFAgf2y7a1kGLDwSzhn', NULL, NULL, 2),
(42, '6reUt', 'AD-eVMZUu', '119.00', '', 'ntngRsfxv7D5JKFS9rbh', NULL, NULL, 2),
(43, 'BHXrO', 'AD-T4sghq', '486.00', '', '6A67Bmh9SnCuGYyFYYN4', NULL, NULL, 4),
(44, 'IF0lW', 'AD-k2CMhn', '921.00', '', '8DtZg3dSkzzkq8kCk9yD', NULL, NULL, 4),
(45, 'lrB4i', 'AD-p2HA7r', '580.00', '', '73vhz9qNUyAGXYyAM8Oz', NULL, NULL, 1),
(46, 'hCU5X', 'AD-AE8ueS', '676.00', '', 'm9WIoa0HvPTE2IBcjWM4', NULL, NULL, 3),
(47, 'NZ1tX', 'AD-UUJjSO', '420.00', '', 'qsZzBstfZBenhu3p2XJD', NULL, NULL, 1),
(48, '4Mao6', 'AD-JzJ1Va', '971.00', '', 'p5TinUSAypl9EjC8bWR8', NULL, NULL, 2),
(49, 'tr840', 'AD-dGCzWv', '906.00', '', 'y3xEnZNBolGsMF6lLC3M', NULL, NULL, 1),
(50, 'LqyeM', 'AD-QBWqqY', '596.00', '', 'W79S6DXQkpwYAMBRgImP', NULL, NULL, 4),
(51, 'HgAba', 'AD-OsxBJg', '923.00', '', 'ULcIpDQ2nm3X1HEhyweq', NULL, NULL, 4),
(52, 'x3eSR', 'AD-FUFzRP', '383.00', '', 'j9Beu2Dlz6VLBYp4uCoD', NULL, NULL, 4),
(53, 'OfpRf', 'AD-bas7y4', '876.00', '', 'Qom2IVl87fZVaaPVoAAB', NULL, NULL, 4),
(54, 'hKnAQ', 'AD-Qm7vo1', '116.00', '', 'okmWDvLv0hWpN5ZVNtM3', NULL, NULL, 3),
(55, 'WC5mm', 'AD-PGXish', '844.00', '', 'Fgv9VLheti14uEJGrGxr', NULL, NULL, 3),
(56, 'NTiVW', 'AD-xsMIDg', '276.00', '', 'aUaZ2nAs1NzEkoqSPWG3', NULL, NULL, 4),
(57, 'z9nsO', 'AD-B0HShK', '394.00', '', '1mA1QgOB9xmhE62HOsDk', NULL, NULL, 2),
(58, 'UVYD1', 'AD-3Wg27C', '451.00', '', 'FvjlXBvjMxRQyZA5MedG', NULL, NULL, 4),
(59, 'E06Uh', 'AD-rUblJl', '653.00', '', 'i0snBz4YM2qG6eZjWuLU', NULL, NULL, 2),
(60, '18P70', 'AD-pRO5ej', '410.00', '', 'MERCl6JXJbRx1oyLi4GK', NULL, NULL, 4),
(61, '32hUV', 'AD-p7dRpO', '614.00', '', 'zHP85C1UOQoQVPiOg6AD', NULL, NULL, 1),
(62, 'xvBzr', 'AD-xF5WEH', '166.00', '', 'KBpgyPmOctsjrWAqm7WH', NULL, NULL, 3),
(63, 'NOAu3', 'AD-73GSpP', '435.00', '', '8cgeNFN9eTZT9JaE7AWG', NULL, NULL, 3),
(64, '1XCfe', 'AD-qwATY9', '384.00', '', '9YPYti424tffuaG4CPzE', NULL, NULL, 3),
(65, 'TMD4A', 'AD-90rkFw', '429.00', '', 'Nrgec94srPQwc0d9OdCh', NULL, NULL, 2),
(66, 'ANc37', 'AD-qTpEVW', '608.00', '', 'Z4Mv01XJOYm2JUmbf73f', NULL, NULL, 1),
(67, 'fRB6e', 'AD-7eSl5S', '603.00', '', 'xfjM3RTfZXNdnTtkAANH', NULL, NULL, 2),
(68, 'Enm4a', 'AD-8lP4Cb', '212.00', '', 'YarUQcZQuuI7sibemy39', NULL, NULL, 4),
(69, 'eshZx', 'AD-XZPPAr', '434.00', '', 'cE3yCkTVLlcGovz7iIZn', NULL, NULL, 3),
(70, 'gasss', 'AD-Vxwvml', '857.00', '', 'XE6eSmchKy2ujenF3RVi', NULL, NULL, 2),
(71, 'l8vFL', 'AD-sM6Cnc', '106.00', '', 'M4pZBb2DG5NsqKKV9Byk', NULL, NULL, 3),
(72, 'xtMcK', 'AD-3r1hdW', '1000.00', '', 'rrNdY1qsfs990H2Pbb2q', NULL, NULL, 3),
(73, 'EPSUn', 'AD-TbihVj', '792.00', '', 'jYW23NZnAfrcyaBhUbcv', NULL, NULL, 1),
(74, 'gCo9M', 'AD-0G1IF7', '718.00', '', 'wPCrmWqHK2cNlUcZc3gh', NULL, NULL, 3),
(75, '0R2xA', 'AD-2x53fE', '587.00', '', 'P8UX2QggRXnrtVpOPGKv', NULL, NULL, 4),
(76, '3Qy3A', 'AD-TkxXZW', '298.00', '', 'sCm8K2IPpor4VEqlfc8B', NULL, NULL, 1),
(77, 'HTDdW', 'AD-k8X4Cp', '243.00', '', '7ddeWkVSV3XeZCD1QWGB', NULL, NULL, 1),
(78, 'zkVdB', 'AD-ArDvbj', '332.00', '', 'fyfl7QtSHThaZNUGlZ1I', NULL, NULL, 2),
(79, 'W86lr', 'AD-YoYZDk', '525.00', '', '0kRVezQxRynfQFCTny1x', NULL, NULL, 3),
(80, 'WB7DU', 'AD-MmYt9l', '415.00', '', 'XmKXYGw5N1cUgfCDcX0R', NULL, NULL, 4),
(81, 'BWlIz', 'AD-TSELIN', '110.00', '', 'f6QQy5CftXXdBIp9zO3g', NULL, NULL, 3),
(82, 'UHMl4', 'AD-AN8dSe', '771.00', '', 'st5DpfAnMFWrx0awPNse', NULL, NULL, 3),
(83, 'tQUmv', 'AD-hdPUur', '729.00', '', 'q1JG8M5a7Ra0w8wxbGJz', NULL, NULL, 2),
(84, 'kiock', 'AD-gwK7VV', '920.00', '', 'KXzMGolAbV01282saUN0', NULL, NULL, 1),
(85, 'UjQbm', 'AD-zY3Hsq', '473.00', '', '0RxuPKvnk7s7ELN05d71', NULL, NULL, 1),
(86, '8BERD', 'AD-mCSik3', '678.00', '', '4tvw6nUKcsuZm4yFTNCV', NULL, NULL, 2),
(87, '72Aya', 'AD-f7qXp7', '225.00', '', 'xajm2erI4wKExfvCId5Z', NULL, NULL, 4),
(88, 'hfbwG', 'AD-HhNldS', '875.00', '', 'VKYUOj9RbguKldOTyhjP', NULL, NULL, 4),
(89, 'ihpNy', 'AD-czQL3o', '273.00', '', 'SsuK7dJC7svPkFLvpR7B', NULL, NULL, 4),
(90, 'p1uA4', 'AD-WqnLjf', '683.00', '', '3NQOUC0knFF8jlDfpIpr', NULL, NULL, 2),
(91, 'ZB1A6', 'AD-OVa8AF', '789.00', '', 'oJqd4dWRkogNFCCZrLYy', NULL, NULL, 2),
(92, 'tVnRl', 'AD-aPWWX8', '140.00', '', 'zi8T7fQywkIJGmJni83Z', NULL, NULL, 1),
(93, 'DdICC', 'AD-JeYmUM', '769.00', '', 'UGd6Gi6Wlp7KOfI0QOFx', NULL, NULL, 2),
(94, '9OY9m', 'AD-Nf3GI1', '877.00', '', 'swXIVVXT4n3akciE16nw', NULL, NULL, 3),
(95, 'rf0sc', 'AD-IXfIm1', '583.00', '', 'gIAWWj3rZVPGTFWdyElP', NULL, NULL, 1),
(96, 'Rdvp1', 'AD-aSGQmK', '513.00', '', 'OXkgCz2BRV1UAoXomhTS', NULL, NULL, 1),
(97, 'BDdeV', 'AD-WDbROc', '972.00', '', 'RjEG73EMGHcaBuTiPate', NULL, NULL, 4),
(98, 'Sugzy', 'AD-Iz5Bhy', '711.00', '', 'zWr7VdC8VmqjpYg7fa7C', NULL, NULL, 1),
(99, 'E4qcz', 'AD-6JF7E3', '646.00', '', 'jfpYqEW66YGi2zH6LXd2', NULL, NULL, 1),
(100, 'ezTSo', 'AD-rXpwsh', '674.00', '', 'bJKkrjifqFjPouIux04W', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `param_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `created_at`, `updated_at`, `param_id`) VALUES
(1, 'Intel', NULL, NULL, '1'),
(2, 'AMD', NULL, NULL, '1'),
(3, 'Asus', NULL, NULL, '2'),
(4, 'Acer', NULL, NULL, '2'),
(5, 'Apple', NULL, NULL, '3'),
(6, 'Samsung', NULL, NULL, '3'),
(7, '12', NULL, NULL, '4'),
(8, '16', NULL, NULL, '4');

-- --------------------------------------------------------

--
-- Table structure for table `tags_products`
--

CREATE TABLE `tags_products` (
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags_products`
--

INSERT INTO `tags_products` (`product_id`, `tag_id`) VALUES
(1, 5),
(1, 7),
(2, 6),
(2, 8),
(3, 2),
(3, 4),
(4, 1),
(4, 3),
(5, 2),
(5, 4),
(6, 5),
(6, 7),
(7, 2),
(7, 4),
(8, 1),
(8, 3),
(9, 2),
(9, 4),
(10, 1),
(10, 3),
(11, 2),
(11, 4),
(12, 6),
(12, 8),
(13, 1),
(13, 3),
(14, 1),
(14, 3),
(15, 2),
(15, 4),
(16, 5),
(16, 7),
(17, 2),
(17, 4),
(18, 1),
(18, 3),
(19, 1),
(19, 3),
(20, 6),
(20, 8),
(21, 5),
(21, 7),
(22, 1),
(22, 3),
(23, 1),
(23, 3),
(24, 2),
(24, 4),
(25, 2),
(25, 4),
(26, 6),
(26, 8),
(27, 1),
(27, 3),
(28, 5),
(28, 7),
(29, 2),
(29, 4),
(30, 2),
(30, 4),
(31, 2),
(31, 4),
(32, 2),
(32, 4),
(33, 2),
(33, 4),
(34, 2),
(34, 4),
(35, 6),
(35, 8),
(36, 1),
(36, 3),
(37, 5),
(37, 7),
(38, 2),
(38, 4),
(39, 2),
(39, 4),
(40, 2),
(40, 4),
(41, 6),
(41, 8),
(42, 2),
(42, 4),
(43, 2),
(43, 4),
(44, 6),
(44, 8),
(45, 2),
(45, 4),
(46, 1),
(46, 3),
(47, 6),
(47, 8),
(48, 2),
(48, 4),
(49, 1),
(49, 3),
(50, 2),
(50, 4),
(51, 1),
(51, 3),
(52, 5),
(52, 7),
(53, 5),
(53, 7),
(54, 1),
(54, 3),
(55, 5),
(55, 7),
(56, 5),
(56, 7),
(57, 1),
(57, 3),
(58, 6),
(58, 8),
(59, 2),
(59, 4),
(60, 6),
(60, 8),
(61, 5),
(61, 7),
(62, 2),
(62, 4),
(63, 2),
(63, 4),
(64, 5),
(64, 7),
(65, 5),
(65, 7),
(66, 6),
(66, 8),
(67, 6),
(67, 8),
(68, 1),
(68, 3),
(69, 5),
(69, 7),
(70, 6),
(70, 8),
(71, 1),
(71, 3),
(72, 6),
(72, 8),
(73, 6),
(73, 8),
(74, 1),
(74, 3),
(75, 6),
(75, 8),
(76, 6),
(76, 8),
(77, 5),
(77, 7),
(78, 6),
(78, 8),
(79, 5),
(79, 7),
(80, 2),
(80, 4),
(81, 1),
(81, 3),
(82, 2),
(82, 4),
(83, 2),
(83, 4),
(84, 6),
(84, 8),
(85, 2),
(85, 4),
(86, 2),
(86, 4),
(87, 6),
(87, 8),
(88, 6),
(88, 8),
(89, 6),
(89, 8),
(90, 2),
(90, 4),
(91, 5),
(91, 7),
(92, 1),
(92, 3),
(93, 5),
(93, 7),
(94, 6),
(94, 8),
(95, 1),
(95, 3),
(96, 1),
(96, 3),
(97, 5),
(97, 7),
(98, 6),
(98, 8),
(99, 1),
(99, 3),
(100, 1),
(100, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'tester', 'test@gmail.com', NULL, '$2y$10$6Rpe4qNwsnTIw7YSSw8cEewOVEd4CDXpU3LfD76HaNoyVgiv/4y5.', 'Ohy9vTDQ0oZYnVltslYdedQcqdvmC62ksd5LK3e8htBNHK4PxMkEaZ5XquT3', '2020-08-10 12:33:00', '2020-08-10 12:33:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `params`
--
ALTER TABLE `params`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `params`
--
ALTER TABLE `params`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
