<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateProduct extends Controller
{
    //
    function createProduct()
    {
        $prod = new Product;
        $prod->tag = Str::random(5);
        $prod->type = Str::random(15);
        $prod->amount = random_int(1,100);
        $prod->price = random_int(1,100);
        $prod->isSold = false;
        $prod->series = Str::random(4);
        $prod->number = Str::random(25);
        $prod->save();
    }

    function index()
    {
        // DB::table('product')->insert([
        //     'tags' => Str::random(5),
        //     'type' => Str::random(15),
        //     'amount' => random_int(1,100),
        //     'price' => random_int(1,100),
        //     'isSold' => false,
        //     'series' => Str::random(4),
        //     'number' => Str::random(25)
        // ]);

        // echo '<br>succes</br>';
    }
}
