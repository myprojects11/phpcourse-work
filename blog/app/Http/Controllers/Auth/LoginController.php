<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function validate_login_form(Request $request)
    {
        $this->validate($request,[
            'email'     => ['required','min:8', 'max:64', 'email'],
            'password'  => ['required','min:8', 'max:32', 'confirmed',
                            'regex: /^.*(?=.{4,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$&+,:;=?@#]).*$/']
            ]);

        $credentials = $request->only('email','password');

        if(Auth::attempt($credentials))
        {
            return redirect('/');
        }
        else
        {
            return back()->with('error','Wrong Login Data');
        }
        //admin@gmail.com
        //abcdE_@123123
    }
}
