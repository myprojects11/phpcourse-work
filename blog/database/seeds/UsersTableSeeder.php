<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'surname' => 'admin',
            'loggin' => 'adminadmin1',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('abcdE_@123123'),
            'remember_token' => Str::random(10),
        ]);
    }
}
