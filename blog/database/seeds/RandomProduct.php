<?php

use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RandomProduct extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('product')->insert([
            'tags' => Str::random(5),
            'type' => Str::random(15),
            'amount' => random_int(1,100),
            'price' => random_int(1,100),
            'isSold' => false,
            'series' => Str::random(4),
            'number' => Str::random(25)
        ]);
    }
}
