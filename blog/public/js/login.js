

var input = document.getElementById('password');
var elem = document.querySelectorAll('.bar');
var idDoneList = document.getElementsByClassName('isDone');
var showPasswordElem =  document.getElementById('toggle-password');


console.dir(showPasswordElem);

showPasswordElem.checked ? showPasswordElem.textContent = '🙈' : showPasswordElem.textContent = '🐵'

const strengthText = ["", "bad 💩", "ok 😐", "decent 🙂", "solid 💪"];
//🙈🐵
let strength = 0;
let showPassword = false;
let disabled = true;
let validations = []

input.oninput = function validatePassword(e) {
    const password = e.target.value;

    validations = [
        (password.length > 8),
        (password.search(/[A-Z]/) > -1),
        (password.search(/[0-9]/) > -1),
        (password.search(/[_$&+,:;=?@#]/) > -1)
    ];

    strength = validations.reduce((acc, cur) => acc + cur, 0);

    console.log(strength);

    //if valid make bars colors
    strength > 0 ? elem[0].classList.add('bar-show') :  elem[0].classList.remove('bar-show');
    strength > 1 ? elem[1].classList.add('bar-show') :  elem[1].classList.remove('bar-show');
    strength > 2 ? elem[2].classList.add('bar-show') :  elem[2].classList.remove('bar-show');
    strength > 3 ? elem[3].classList.add('bar-show') :  elem[3].classList.remove('bar-show');

    //if valid toggle icons
    validations[0] ? idDoneList[0].textContent = '✔️' : idDoneList[0].textContent = '❌';
    validations[1] ? idDoneList[1].textContent = '✔️' : idDoneList[1].textContent = '❌';
    validations[2] ? idDoneList[2].textContent = '✔️' : idDoneList[2].textContent = '❌';
    validations[3] ? idDoneList[3].textContent = '✔️' : idDoneList[3].textContent = '❌';


    console.log(idDoneList);
}
