@extends('layouts.auth')

@section('form')
<form method="POST" action="{{ url('/') }}">
    @csrf
    <div class="user-data">
        <div class="field field-name">
            <input
                name="user-name"
                class="input"
                placeholder=" " />
            <label for="email" class="label">Name</label>
        </div>
        <div class="field field-surname">
        <input
            name="user-surname"
            class="input"
            placeholder=" " />
        <label for="email" class="label">Surname</label>
        </div>
    </div>


    <div class="field">
        <input
            name="user-login"
            class="input"
            placeholder=" " />
        <label for="email" class="label">Login</label>
    </div>

    <div class="field">
        <input
        type="email"
        name="email"
        class="input"
        placeholder=" " />
        <label for="email" class="label">Email</label>
    </div>

    <div class="field">

        <input
        id='password'
        {{-- type="password" --}}
        name="password"
        class="input"
        placeholder=" "
        oninput(validatePassword)
        />
        <label for="password" class="label">Password</label>
        <input type="checkbox" id="toggle-password" />
    </div>

    <div class="field">

        <input
            id='cpassword'
            {{-- type="password" --}}
            name="password_confirmation"
            class="input"
            placeholder=" "
            />
        <label for="password" class="label">Password</label>
        <input type="checkbox" id="toggle-password" />
        </div>

    <div class="strength">
        <span class="bar bar-1" ></span>
        <span class="bar bar-2" ></span>
        <span class="bar bar-3" ></span>
        <span class="bar bar-4" ></span>
    </div>

    <ul id="strenght-level">
        <li><span class="isDone">❌</span> must be at least 8 characters</li>
        <li><span class="isDone">❌</span> must contain a capital letter</li>
        <li><span class="isDone">❌</span> must contain a number</li>
        <li><span class="isDone">❌</span> must contain one of $&+,:;=?@#</li>
    </ul>

    <!-- <div class="strength-text">{strengthText[strength]}</div> -->

    <button class="my_button">Create</button>

</form>
@endsection


