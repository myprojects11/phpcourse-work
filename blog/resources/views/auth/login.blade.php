@extends('layouts.auth')

@section('form')
{{-- action="{{ url('/') }}" --}}
<form method="POST">
    @csrf

    <div class="field">
    <input
        type="email"
        name="email"
        class="input"
        placeholder=" " />
    <label for="email" class="label">Email</label>
    </div>

    <div class="field">

    <input
        id='password'
        {{-- type="password" --}}
        name="password"
        class="input"
        placeholder=" "
        oninput(validatePassword)
        />
    <label for="password" class="label">Password</label>
    <input type="checkbox" id="toggle-password" />
    </div>

    <!-- <div class="strength-text">{strengthText[strength]}</div> -->

    <button class="my_button">Sign Up</button>

</form>
@endsection




